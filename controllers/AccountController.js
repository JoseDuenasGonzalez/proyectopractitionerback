const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getAccountV2(req, res) {
 console.log("GET /apitechu/v2/accounts/:userid");

 var id = Number.parseInt(req.params.userid);
 console.log("La id de usuario para cuentas es " + id);
 var query = "q=" + JSON.stringify({"idUser": id,"status":"V"});
 console.log("Query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("account?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     console.log(body);
     if (err) {
       response = {
         "msg" : "Error obteniendo cuentas."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body;
       } else {
         response = {
           "msg" : "El usuario no tiene cuentas."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
 );
}

function createAccount(req, res) {
  console.log("POST /apitechu/v2/accounts");

  console.log(req.body.IBAN);
  console.log(req.body.idUser);
  console.log(req.body.balance);

  var iban = "ES99 0182 0001 " + Math.round(getRandomNumber(999,9999)) + " " +  Math.round(getRandomNumber(999,9999)) + " " +  Math.round(getRandomNumber(999,9999));

  var newAccount = {
    "IBAN" : iban,
    "idUser" : req.body.idUser,
    "balance" : 0,
    "status" : "V"
  }

  console.log(newAccount);


  var httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("account?" + mLabAPIKey, newAccount,
    function(err, resMlab, body) {
      console.log("Cuenta creada");
      res.status(201).send({"msg":"Cuenta creada"});
    }
  );

}


function cancelAccount(req, res) {
 console.log("Cancel account")
 console.log("POST /apitechu/v2/accounts/:IBAN");
 console.log("IBAN " + req.params.iban);

 var query = "q=" + JSON.stringify({"IBAN": req.params.iban});
 console.log("query es " + query);
 var httpClient = requestJson.createClient(baseMLabURL);
 var putBody = '{"$unset":{"status":""}}'
 httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
   function(err, resMLab, body) {
     console.log("Cuenta cancelada");
     var response = {
       "msg" : "Cuenta cancelada"
     }
     res.send(response);
   }
 );
}




function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

module.exports.getAccountV2 = getAccountV2;
module.exports.createAccount = createAccount;
module.exports.cancelAccount = cancelAccount;
