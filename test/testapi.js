const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');

chai.use(chaihttp);

/*Should es el framework de aserciones*/
var should = chai.should();

/*Cada it es un test unitario*/
describe("First test",
  function() {
    it('Tests that duckduckgo works', function(done) {
        chai.request('http://www.ducduckgo.com')
            .get('/')
            .end(
              function (err, res) {
                console.log("Request finished");
                /*console.log(res); */
                /*console.log(err); */
                /*Ponemos el done para que el sistema sepa que
                ya hemos hecho todo lo que tenemos que hacer y puede
                evaluar*/
                done();
              }
            )
    }
   )
  }

)

describe("Test de API de usuarios",
  function() {
    it('Tests that user api says hello', function(done) {
        chai.request('http://localhost:3000')
            .get('/apitechu/v1/hello')
            .end(
              function (err, res) {
                console.log("Request finished");
                res.should.have.status(200);
                /*Aquí evaluamos la respuesta de la petición Hello
                en el body, podemos ver en postman lo que devolvemos*/
                res.body.msg.should.be.eql("Hola desde API TechU");
                done();
              }
            )
    }
   ),
   it('Tests that user api returns user list', function(done) {
       chai.request('http://localhost:3000')
           .get('/apitechu/v1/users')
           .end(
             function (err, res) {
               console.log("Request finished");
               res.should.have.status(200);
               res.body.users.should.be.a('array');

               for (user of res.body.users) {
                 user.should.have.property('id');
                 user.should.have.property('email');
                 user.should.have.property('password');
               }
               done();
             }
           )
   }
  )
  }

)
